package cup.opslogicas;

import cup.expresionesregulares.ExpReg;
public class While implements Operador{
  private ExpReg exp;
  private Operador op;
  
  public While(ExpReg e, Operador o){
    exp=e;
    op=o;
  }
  
  @Override
  public void operacion() {
   while(exp.expresion()){
     op.operacion();
   } 
  }
  
}
