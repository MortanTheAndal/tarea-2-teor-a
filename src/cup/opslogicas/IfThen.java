package cup.opslogicas;

import cup.expresionesregulares.ExpReg;

public class IfThen implements Operador  {
  ExpReg exp;
  Operador op;
  
  public IfThen(ExpReg e, Operador o){
    exp=e;
    op = o;
  }

  @Override
  public void operacion() {
    if(exp.expresion()){
      op.operacion();
    }
  }

}
