package cup.opslogicas;

import cup.expresionesregulares.ExpReg;

public class IfElse implements Operador  {
  ExpReg exp;
  Operador op1;
  Operador op2;
  
  public IfElse(ExpReg e, Operador func1, Operador func2){
    exp = e;
    op1 = func1;
    op2 = func2;
  }

  @Override
  public void operacion() {
    if(exp.expresion()){
      op1.operacion();
    }
    else{
      op2.operacion();
    }
  }

}

