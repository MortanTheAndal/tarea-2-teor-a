package cup.expresionesregulares;

public class YLogico implements ExpReg {
  private ExpReg expr1;
  private ExpReg expr2; 
  public YLogico(Object a, Object b){
    expr1=(ExpReg)a;
    expr2=(ExpReg)b;
  }
  @Override
  public boolean expresion() {
    return expr1.expresion() && expr2.expresion();
  }

}
