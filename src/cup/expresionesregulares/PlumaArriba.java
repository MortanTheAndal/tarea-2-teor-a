package cup.expresionesregulares;

import cup.example.Pluma;

public class PlumaArriba implements ExpReg {
  Pluma pluma;
  
  public PlumaArriba(Pluma pl){
    pluma = pl;
  }
  
  @Override
  public boolean expresion() {
    // TODO Auto-generated method stub
    return pluma.getModo()==false;
  }

}
