package cup.expresionesregulares;

public class Negacion implements ExpReg {
  private ExpReg expr1;
  public Negacion(Object a){
    expr1=(ExpReg)a;
  }
  @Override
  public boolean expresion() {
    return !expr1.expresion();
  }

}
