package cup.expresionesregulares;

import cup.example.Pluma;
public class PlumaDireccion implements ExpReg {

  Pluma pluma;
  char cmpDir;
  
  public PlumaDireccion(Pluma pl, char dir){
    pluma= pl;
    cmpDir=dir;
  }
  @Override
  public boolean expresion() {
    // TODO Auto-generated method stub
    return pluma.getDir()==cmpDir;
  }

}
