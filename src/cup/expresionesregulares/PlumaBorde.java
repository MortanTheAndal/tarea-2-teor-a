package cup.expresionesregulares;

import cup.example.Pluma;

public class PlumaBorde implements ExpReg {
  Pluma pluma;
  
  public PlumaBorde(Pluma pl){
    pluma = pl; 
  }
  @Override
  public boolean expresion() {
    // TODO Auto-generated method stub
    return pluma.getBorde();
  }

}
