package cup.expresionesregulares;
import cup.example.Pluma;
public class PlumaColor implements ExpReg {
  Pluma pluma;
  char cmpColor;
  
  public PlumaColor(Pluma pl, char col){
    pluma= pl ;
    cmpColor=col; 
  }
  
  @Override
  public boolean expresion() {
    // TODO Auto-generated method stub
    return pluma.getColor()==cmpColor;
  }

}
