package cup.expresionesregulares;

import cup.example.Pluma;

public class PlumaAbajo implements ExpReg {
  Pluma pluma; 
  
  public PlumaAbajo(Pluma pl){
    pluma = pl; 
  }

  @Override
  public boolean expresion() {
    // TODO Auto-generated method stub
    return pluma.getModo()==true;
  }
  
}
